provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}
provider "aws" {
  alias      = "ses_email_region"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.email_region}"
}


output "web_bucket_url" {
  value = "${format("%s/%s", aws_s3_bucket.static_web.bucket_domain_name, aws_s3_bucket_object.static_web_index_file.key)}"
}

output "cloudfront_domain" {
  value = "${aws_cloudfront_distribution.s3_distribution.domain_name}"
}
