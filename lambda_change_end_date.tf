
resource "aws_lambda_function" "lambda_change_end_date" {
  function_name    = "lambda_change_end_date_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "lambda_change_end_date.lambda_handler"
  runtime          = "python3.6"
  timeout          = 60
  role             = "${aws_iam_role.lambda_change_end_date_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

data "aws_iam_policy_document" "lambda_change_end_date_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda_change_end_date_permissions_policy" {
  statement {
    sid = "SSMPermissions${var.env}${var.app_version}"
    actions = [
      "ssm:GetParameter"
    ]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}",
      "${aws_ssm_parameter.dw_admin_emails.arn}",
      "${aws_ssm_parameter.ses_source_email_address.arn}",
      "${aws_ssm_parameter.ses_template_end_date_updated_admins.arn}",
      "${aws_ssm_parameter.ses_template_end_date_updated_user.arn}",
      "${aws_ssm_parameter.secret_key.arn}"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:UpdateItem"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
  statement {
    sid = "WorkspacesPermissions${var.env}${var.app_version}"
    actions = [
      "workspaces:DescribeWorkspaces",
      "workspaces:CreateTags",
      "workspaces:DescribeTags",
    ]
    resources = ["*"]
  }
  statement {
    sid = "SESpermissions${var.env}${var.app_version}"
    actions = [
      "ses:SendTemplatedEmail"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda_change_end_date_policy" {
  name        = "lambda_change_end_date_policy_${var.env}_${var.app_version}"
  description = "Policy for changing end date lambda, Digital Workforce"

  policy = "${data.aws_iam_policy_document.lambda_change_end_date_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_change_end_date_attach" {
  role       = "${aws_iam_role.lambda_change_end_date_assume_role.name}"
  policy_arn = "${aws_iam_policy.lambda_change_end_date_policy.arn}"
}

resource "aws_iam_role" "lambda_change_end_date_assume_role" {
  name = "lambda_change_end_date_role_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_change_end_date_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

resource "aws_iam_role_policy_attachment" "lambda_change_end_date_logs_attach" {
  role       = "${aws_iam_role.lambda_change_end_date_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}

