
resource "aws_lambda_function" "lambda_fetch_end_dates" {
  function_name    = "lambda_fetch_end_dates_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "lambda_fetch_end_dates.lambda_handler"
  runtime          = "python3.6"
  timeout          = 60
  role             = "${aws_iam_role.lambda_fetch_end_dates_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

data "aws_iam_policy_document" "lambda_fetch_end_dates_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda_fetch_end_dates_permissions_policy" {
  statement {
    sid = "SSMPermissions${var.env}${var.app_version}"
    actions = [
      "ssm:GetParameter"
    ]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:Scan"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
}

resource "aws_iam_policy" "lambda_fetch_end_dates_policy" {
  name        = "lambda_fetch_end_dates_policy_${var.env}_${var.app_version}"
  description = "Policy for fetching end dates lambda, Digital Workforce"

  policy = "${data.aws_iam_policy_document.lambda_fetch_end_dates_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_fetch_end_dates_attach" {
  role       = "${aws_iam_role.lambda_fetch_end_dates_assume_role.name}"
  policy_arn = "${aws_iam_policy.lambda_fetch_end_dates_policy.arn}"
}

resource "aws_iam_role" "lambda_fetch_end_dates_assume_role" {
  name = "lambda_fetch_end_dates_role_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_fetch_end_dates_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

resource "aws_iam_role_policy_attachment" "lambda_fetch_end_dates_logs_attach" {
  role       = "${aws_iam_role.lambda_fetch_end_dates_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}

