/**
*   Create buckets
*/

resource "aws_s3_bucket" "static_web" {
  bucket = "${var.static_web_bucket_name}"
  acl    = "public-read"
  website {
    index_document = "index.html"
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET"]
    allowed_origins = ["*"]
  }
}

resource "aws_s3_bucket_policy" "static_web_bucket_policy" {
  bucket = "${aws_s3_bucket.static_web.id}"
  policy = "${data.aws_iam_policy_document.static_web_bucket_policy_document.json}"
}

data "aws_iam_policy_document" "static_web_bucket_policy_document" {
  /*
  statement {
    sid = "bucket_policy_static_web"
    effect = "Allow"
    actions = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.static_web.arn}/*"]
    principals = {
      type = "*"
      identifiers = ["*"]
    }
    condition = {
      test = "IpAddress"
      variable = "aws:SourceIp"
      values = ["${var.whitelisted_ips}"]
    }
    condition = {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["true"]
    }
  }
  */
  statement {
    sid = "cloudFrontAccess_${var.env}_${var.app_version}"
    effect = "Allow"
    actions = ["s3:GetObject"]
    principals = {
      type = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn}"]
    }
    resources = ["${aws_s3_bucket.static_web.arn}/*"]
  }
  statement {
    actions   = ["s3:ListBucket"]
    resources = ["${aws_s3_bucket.static_web.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn}"]
    }
  }
}

resource "aws_s3_bucket" "lambdas_bucket" {
  bucket = "${var.lambdas_bucket}"
  acl = "private"
}

/*
*   Web index file
*/

resource "aws_s3_bucket_object" "static_web_index_js_file" {
  depends_on   = ["null_resource.replace_angular_js_values"]
  bucket       = "${aws_s3_bucket.static_web.id}"
  key          = "index.js"
  content_type = "application/javascript"
  source       = "${var.static_web_local_folder}/index.js"
  #etag         = "${md5(file("${var.angular_dist_folder}/distr.js"))}"
  #content      = "${data.template_file.static_web_index_js_file_content.rendered}"
}

resource "aws_s3_bucket_object" "static_web_index_file" {
  depends_on   = ["null_resource.replace_angular_html_values"]
  bucket       = "${aws_s3_bucket.static_web.id}"
  key          = "index.html"
  content_type = "text/html"
  source       = "${var.static_web_local_folder}/index.html"
  #etag         = "${md5(file("${var.angular_dist_folder}/index.html"))}"
  #content      = "${data.template_file.static_web_index_file_content.rendered}"
}

resource "null_resource" "copy_angular_distr_files" {
  provisioner "local-exec" {
    command = <<EOT
      cp ${var.angular_dist_folder}/index.html ${var.static_web_local_folder}/index.html &&
      cp ${var.angular_dist_folder}/distr.js ${var.static_web_local_folder}/index.js
EOT
  }
}

resource "null_resource" "replace_angular_html_values" {
  depends_on = ["null_resource.copy_angular_distr_files", "aws_s3_bucket_object.static_web_index_js_file"]
  provisioner "local-exec" {
    command = <<EOT
      sed -i 's|terra_js_file_location|${aws_s3_bucket_object.static_web_index_js_file.key}|g' ${var.static_web_local_folder}/index.html
EOT
  }
}

resource "null_resource" "replace_angular_js_values" {
  depends_on = ["null_resource.copy_angular_distr_files", "aws_api_gateway_rest_api.gateway_rest_api"]
  provisioner "local-exec" {
    command = <<EOT
      sed -i 's|terra_end_dates_url|${aws_api_gateway_deployment.gateway_deployment.invoke_url}/${var.end_date_api_end}|g' ${var.static_web_local_folder}/index.js &&
      sed -i 's|terra_new_workspace_post_url|${aws_api_gateway_deployment.gateway_deployment.invoke_url}/${var.workspaces_api_end}|g' ${var.static_web_local_folder}/index.js &&
      sed -i 's|terra_change_end_date_url|${aws_api_gateway_deployment.gateway_deployment.invoke_url}/${var.change_end_date_api_end}|g' ${var.static_web_local_folder}/index.js &&
      sed -i 's|terra_directory_id|${var.directory_id}|g' ${var.static_web_local_folder}/index.js &&
      sed -i 's|terra_bundle_ids|${join(",", formatlist("%s", var.bundle_ids))}|g' ${var.static_web_local_folder}/index.js
EOT
  }
}

/*
data "template_file" "static_web_index_file_content" {
  template = "${file("${var.angular_dist_folder}/index.html")}"

  vars {
    js_file_location        = "${aws_s3_bucket_object.static_web_index_js_file.key}"
  }
}

data "template_file" "static_web_index_js_file_content" {
  template = "${file("${var.angular_dist_folder}/distr.js")}"

  vars {
    end_dates_url           = "${aws_api_gateway_deployment.gateway_deployment.invoke_url}/${var.end_date_api_end}"
    new_workspace_post_url  = "${aws_api_gateway_deployment.gateway_deployment.invoke_url}/${var.workspaces_api_end}"
    change_end_date_url     = "${aws_api_gateway_deployment.gateway_deployment.invoke_url}/${var.change_end_date_api_end}"
    directory_id            = "${var.directory_id}"
    #bundle_ids_test         = "${join("", formatlist("<option value='%s'>%s</option>", var.bundle_ids, var.bundle_ids))}"
    #bundle_ids_test         = "${join(",", formatlist("%s", var.bundle_ids))}"
    bundle_ids_test         = "test"
  }
}
*/

/*
*   Cloudformation file
*/
resource "aws_s3_bucket_object" "workspace_cf_template" {
  bucket = "${aws_s3_bucket.lambdas_bucket.id}"
  key    = "workspace_cloudformation.yaml"
  source = "${var.lambda_bucket_local_folder}/workspace_cloudformation.yaml"
}

/*
*   Lambda functions
*/
resource "aws_s3_bucket_object" "lambda_code" {
  bucket = "${aws_s3_bucket.lambdas_bucket.id}"
  key    = "v${var.app_version}/code.zip"
  source = "code.zip"
  depends_on = ["data.archive_file.lambda_code_zip_file"]
}

data "archive_file" "lambda_code_zip_file" {
  type        = "zip"
  output_path = "code.zip"
  source_dir  = "${var.lambda_bucket_local_folder}"
}
