resource "aws_lambda_function" "lambda_expiration_checker" {
  function_name    = "lambda_expiration_checker_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "lambda_expiration_checker.lambda_handler"
  runtime          = "python3.6"
  timeout          = 10
  role             = "${aws_iam_role.lambda_expiration_checker_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

/*
*   IAM
*/

resource "aws_iam_role" "lambda_expiration_checker_assume_role" {
  name = "lambda_expiration_checker_role_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_expiration_checker_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

data "aws_iam_policy_document" "lambda_expiration_checker_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "lambda_expiration_checker_policy" {
  name        = "lambda_expiration_checker_policy_${var.env}_${var.app_version}"
  description = "Policy for expiration checking Lambda, Digital Workforce"

  policy = "${data.aws_iam_policy_document.lambda_expiration_checker_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_expiration_checker_attach" {
  role       = "${aws_iam_role.lambda_expiration_checker_assume_role.name}"
  policy_arn = "${aws_iam_policy.lambda_expiration_checker_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "lambda_expiration_checker_logs_attach" {
  role       = "${aws_iam_role.lambda_expiration_checker_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}

data "aws_iam_policy_document" "lambda_expiration_checker_permissions_policy" {
  statement {
    sid = "SQSPermissions${var.env}${var.app_version}"
    actions = ["sqs:SendMessage"]
    resources = [
      "${aws_sqs_queue.ses_email_queue.arn}"
    ]
  }
  statement {
    sid = "SSMPermissions${var.env}${var.app_version}"
    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters"
    ]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}",
      "${aws_ssm_parameter.dw_email_sqs.arn}"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:GetItem",
      "dynamodb:Scan"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
}

resource "aws_lambda_permission" "lambda_expiration_checker_exec_from_cloudwatch" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda_expiration_checker.function_name}"
    principal = "events.amazonaws.com"
    source_arn = "${aws_cloudwatch_event_rule.expiration_check_every_day.arn}"
}

