var concat = require('concat-files');
var compile_to = './dist/distr.js';

var filesArray = [
    './dist/my-app/runtime.js',
    './dist/my-app/es2015-polyfills.js',
    './dist/my-app/polyfills.js',
    './dist/my-app/styles.js',
    './dist/my-app/main.js'
];

concat(
    filesArray,
    compile_to,
    function(err) {
        if (err) throw err;
        console.log('Compiled to , ', compile_to);
    }
);
