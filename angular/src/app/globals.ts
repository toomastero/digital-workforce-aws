import { Injectable } from '@angular/core';

@Injectable()
export class Globals {

    // Variablaes
    directoryId: string = "terra_directory_id";
    bundleIds: Array<string> = "terra_bundle_ids".split(",");

    // URLs
    newWorkspacePostUrl: string = "terra_new_workspace_post_url";
    changeEndDatePostUrl: string = "terra_change_end_date_url";
    endDatesFetchUrl: string = "terra_end_dates_url";
}
