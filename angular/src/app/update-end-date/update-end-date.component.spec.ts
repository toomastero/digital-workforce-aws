import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEndDateComponent } from './update-end-date.component';

describe('UpdateEndDateComponent', () => {
  let component: UpdateEndDateComponent;
  let fixture: ComponentFixture<UpdateEndDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEndDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEndDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
