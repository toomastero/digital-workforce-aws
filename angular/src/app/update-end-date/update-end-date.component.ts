import { Component, OnInit, ViewChild } from '@angular/core';
import { Globals } from './../globals';
import { UpdateEndDateWorkspace } from '../models/update-end-date-workspace';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';


class Workspace {
    startDate: string;
    username: string;
    endDate: string;
    useremail: string;
}

@Component({
    selector: 'app-update-end-date',
    templateUrl: './update-end-date.component.html',
    styleUrls: ['./update-end-date.component.css']
})
export class UpdateEndDateComponent implements OnInit {
    @ViewChild('instance') instance: NgbTypeahead;
    focus$ = new Subject<string>();
    click$ = new Subject<string>();

    changeEndDatePostUrl: string;
    endDatesFetchUrl: string;
    secretKey: string;

    workspaces: Array<UpdateEndDateWorkspace> = [];
    selectedWorkspace: UpdateEndDateWorkspace = {
        useremail: "",
        startDate: "",
        oldEndDate: "",
        newEndDate: "",
        username: ""
    };
    loading: boolean = true;

    constructor(private globals: Globals, private http: HttpClient) {
        this.changeEndDatePostUrl = globals.changeEndDatePostUrl;
        this.endDatesFetchUrl = globals.endDatesFetchUrl;
    }

    formatter = (x: UpdateEndDateWorkspace) => x.useremail;

    workspacesSearch = (text: Observable<string>) => {
        const debouncedText$ = text.pipe(debounceTime(200), distinctUntilChanged());
        const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
        const inputFocus$ = this.focus$;
        const maxValue: number = 20;

        return merge(debouncedText$, clicksWithClosedPopup$, inputFocus$).pipe(
          map(term =>
              (term === '' ? this.workspaces :
                  this.workspaces.filter(w => w.useremail.toLowerCase().indexOf(term.toLowerCase()) > -1))
              .slice(0, maxValue)
          )
        )
    }

    ngOnInit() {
        this.loading = true;
        this.http.get<Workspace[]>(this.endDatesFetchUrl)
            .pipe(
                map(workspaces => {
                    return workspaces.map(workspace => {
                        return {
                            useremail: workspace.useremail,
                            startDate: workspace.startDate,
                            oldEndDate: workspace.endDate,
                            newEndDate: "",
                            username: workspace.username
                        };
                    });
                })
            )
            .subscribe(data => {
                this.workspaces = data
                this.loading = false;
            });
    }

}
