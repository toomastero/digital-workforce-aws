import { Component, OnInit } from '@angular/core';
import { NewWorkspace } from '../models/new-workspace';
import { NgbDatepickerConfig, NgbDateNativeUTCAdapter } from '@ng-bootstrap/ng-bootstrap';
import { Globals } from './../globals';
import * as dateFns from 'date-fns';


@Component({
    selector: 'app-new-workspace',
    templateUrl: './new-workspace.component.html',
    styleUrls: ['./new-workspace.component.css'],
    providers: [
        {provide: NgbDatepickerConfig, useClass: NgbDateNativeUTCAdapter}
    ]
})
export class NewWorkspaceComponent implements OnInit {
    newWorkspacePostUrl: string;
    directoryId: string;
    bundleIds: Array<string>;
    secretKey: string;

    workspace: NewWorkspace = {
        username: "",
        useremail: "",
        password: "",
        fullName: "",
        startDate: {},
        endDate: {},
        bundleId: "",
        directoryId: "",
        secretKey: ""
    };

    constructor(config: NgbDatepickerConfig, private globals: Globals) {
        config.showWeekNumbers = true;
        this.newWorkspacePostUrl = globals.newWorkspacePostUrl;
        this.directoryId = globals.directoryId;
        this.bundleIds = globals.bundleIds;

        this.workspace.directoryId = globals.directoryId;
    }

    ngOnInit() { }

    onBlurMethod() {
        let newBundleId: string = this.workspace.bundleId;
        if (!this.bundleIds.includes(newBundleId)) {
            this.bundleIds.push(newBundleId);
        }
    }

    onStartDateChange(newStartDate: any) {
        let endDaysAmount: number = 30;

        let startDate =  newStartDate.year + "-" + newStartDate.month + "-" + newStartDate.day;
        let parsedFns = dateFns.parse(startDate);
        let newEndDate = dateFns.addDays(parsedFns, endDaysAmount);

        this.workspace.endDate = {
            year: Number(dateFns.format(newEndDate, "YYYY")),
            month: Number(dateFns.format(newEndDate, "M")),
            day: Number(dateFns.format(newEndDate, "D"))
        };
    }
}
