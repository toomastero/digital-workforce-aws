import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Bootstrap
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
// Font Awesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCalendarAlt  } from '@fortawesome/free-regular-svg-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewWorkspaceComponent } from './new-workspace/new-workspace.component';
import { Globals } from './globals';
import { UpdateEndDateComponent } from './update-end-date/update-end-date.component';
import { NgbDateCustomParserFormatter } from './formatters/NgbDateCustomParserFormatter';
import { environment } from '../environments/environment';



@NgModule({
    declarations: [
        NewWorkspaceComponent,
        AppComponent,
        UpdateEndDateComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        NgbModule,
        FontAwesomeModule,
        HttpClientModule
    ],
    providers: [
        Globals,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}

    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
    constructor(private globals: Globals) {
        // Add an icon to the library for convenient access in other components
        library.add(faCalendarAlt);

        if (!environment.production) {
            globals.endDatesFetchUrl = "https://teb1hy8xff.execute-api.eu-west-1.amazonaws.com/prod/end-dates";
            globals.directoryId = "d-12313ubr9u31";
            globals.bundleIds = ["bundle1", "bundle2"];
        }
      }
}
