export class NewWorkspace {
    username: string = "";
    useremail: string = "";
    password: string = "";
    fullName: string = "";
    startDate: object = {};
    endDate: object = {};
    bundleId: string = "";
    directoryId: string = "";
    secretKey: string = "";
}
