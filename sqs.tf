resource "aws_sqs_queue" "ses_email_queue" {
  name                      = "ses_email_queue_${var.env}_${var.app_version}"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
}
