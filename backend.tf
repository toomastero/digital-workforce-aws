
# DW

terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-ofb9ub17f" // Bucket name, DW
    key     = "terraform-state"
    region  = "eu-west-1" # DW
  }
}


/*
terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-r41t1h01" // Bucket name, For testing
    key     = "terraform-state"
    region  = "eu-central-1" # For testing
  }
}
*/
