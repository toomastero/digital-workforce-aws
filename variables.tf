data "aws_caller_identity" "current" {}

variable "access_key" {}
variable "secret_key" {}

variable "region" {
  type = "string"
  default = "eu-west-1" # DW
}
variable "email_region" {
  type = "string"
  default = "eu-west-1"
}
variable "app_version" {
  type = "string"
  default = "1"
}
variable "env" {
  default = "test"
}


// TERRAFORM STATE

variable dynamodb_for_customers_state {
  type = "string"
  default = "workspaces-customer-state" // DynamoDB table name
}

// SECURITY

variable "secret_key_value" {
  default = "DWFacademy2019"
}

variable "whitelisted_ips" {
  type = "list"
  default = [
    "62.165.150.43", # Nordcloud office
    "91.154.127.130", # Toomas' house
    "62.183.135.194", # From Jere Vekka
    "85.76.78.236", # From Annika, 30.01.2019
    "52.212.230.104", # Nordcloud VPN, 30.01.2019
    "147.231.47.170", # From Annika, 11.02.2019
    "85.76.78.250", # From Annika, 11.02.2019
    "85.76.75.208", # From Annika, 21.02.2019
    "85.76.98.44" # From Annika, 26.02.2019
  ]
}

// WORKSPACES

variable directory_id {
  type = "string"
  default = "d-9367222986" # DW
}

variable bundle_ids {
  type = "list"
  default = [
    "wsb-xlj4bcn7s" # DW
  ]
}

// NAMES

variable dynamodb_for_state {
  type = "string"
  default = "toomas-terraform-dw-testing" // DynamoDB table name
}
variable workspace_cloudformation_template {
  type = "string"
  default = "cf_templates/workspace_cloudformation.yaml"
}

// S3

variable "static_web_bucket_name" {
  default = "terraform-static-web-bucket-hd13h9t31" // Bucket name, DW
}
variable "lambdas_bucket" {
  default = "terraform-lambdas-bucket-nwo1397d" // Bucket name, DW
}
variable "static_web_local_folder" {
  default = "static_web_bucket"
}
variable "angular_dist_folder" {
  default = "angular/dist"
}
variable "lambda_bucket_local_folder" {
  default = "lambda_codes"
}

// SES email addresses

variable "ses_source_email_address_var" {
  default = "info@dwfacademy.com" # DW
}
variable "dw_admin_emails_var" {
  type = "list"
  default = ["info@dwfacademy.com"] # DW
}

// API Gateway
variable "api_gateway_stage_name" {
  default = "prod" # DW
}

// Passwords

variable "password_prefix" {
  default = "password"
}

