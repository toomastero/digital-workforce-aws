resource "aws_ssm_parameter" "dw_dynamodb_table" {
  name  = "dw_dynamodb_table"
  type  = "String"
  value = "${data.aws_dynamodb_table.dynamodb_table.id}"
}

resource "aws_ssm_parameter" "dw_email_sqs" {
  name  = "dw_email_sqs"
  type  = "String"
  value = "${aws_sqs_queue.ses_email_queue.id}"
}

resource "aws_ssm_parameter" "dw_cloudformation_sns_topic" {
  name  = "dw_cloudformation_sns_topic"
  type  = "String"
  value = "${aws_sns_topic.cloudformation_workspaces_sns_topic.arn}"
}

resource "aws_ssm_parameter" "dw_cloudformation_template_name" {
  name  = "dw_cloudformation_template_name"
  type  = "String"
  value = "${aws_s3_bucket_object.workspace_cf_template.id}"
}

resource "aws_ssm_parameter" "dw_cloudformation_template_bucket_name" {
  name  = "dw_cloudformation_template_bucket_name"
  type  = "String"
  value = "${aws_s3_bucket.lambdas_bucket.id}"
}

// SES email addresses

resource "aws_ssm_parameter" "ses_source_email_address" {
  name  = "ses_source_email_address"
  type  = "String"
  value = "${var.ses_source_email_address_var}"
}

resource "aws_ssm_parameter" "dw_admin_emails" {
  name  = "dw_admin_emails"
  type  = "StringList"
  value = "${join(",", var.dw_admin_emails_var)}"
}

// SES

resource "aws_ssm_parameter" "ses_template_on_creation_successful" {
  provider = "aws.ses_email_region"
  name  = "ses_template_on_creation_successful"
  type  = "String"
  value = "${aws_ses_template.OnCreationSuccessful.id}"
}

resource "aws_ssm_parameter" "ses_template_on_creation_failed" {
  provider = "aws.ses_email_region"
  name  = "ses_template_on_creation_failed"
  type  = "String"
  value = "${aws_ses_template.OnCreationFailed.id}"
}

resource "aws_ssm_parameter" "ses_template_on_deletion_successful" {
  provider = "aws.ses_email_region"
  name  = "ses_template_on_deletion_successful"
  type  = "String"
  value = "${aws_ses_template.OnDeletionSuccessful.id}"
}

resource "aws_ssm_parameter" "ses_template_expiration_days_left" {
  provider = "aws.ses_email_region"
  name  = "ses_template_expiration_days_left"
  type  = "String"
  value = "${aws_ses_template.WorkspaceExpirationEmail.id}"
}

resource "aws_ssm_parameter" "ses_template_end_date_updated_admins" {
  provider = "aws.ses_email_region"
  name  = "ses_template_end_date_updated_admins"
  type  = "String"
  value = "${aws_ses_template.EndDateUpdatedToAdmins.id}"
}

resource "aws_ssm_parameter" "ses_template_end_date_updated_user" {
  provider = "aws.ses_email_region"
  name  = "ses_template_end_date_updated_user"
  type  = "String"
  value = "${aws_ses_template.EndDateUpdatedToClient.id}"
}

resource "aws_ssm_parameter" "ses_template_on_creation_successful_email_to_client" {
  provider = "aws.ses_email_region"
  name  = "ses_template_on_creation_successful_email_to_client"
  type  = "String"
  value = "${aws_ses_template.OnCreationSuccessfulEmailToClient.id}"
}

// Passwords

resource "aws_ssm_parameter" "password_prefix" {
  name  = "password_prefix"
  type  = "String"
  value = "${var.password_prefix}"
}

// Security

resource "aws_ssm_parameter" "secret_key" {
  name  = "secret_key"
  type  = "String"
  value = "${var.secret_key_value}"
}
