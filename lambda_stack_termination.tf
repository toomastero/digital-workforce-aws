resource "aws_lambda_function" "lambda_stack_termination" {
  function_name    = "lambda_stack_termination_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "lambda_stack_termination.lambda_handler"
  runtime          = "python3.6"
  timeout          = 60
  role             = "${aws_iam_role.lambda_stack_termination_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

/*
*   IAM
*/

resource "aws_iam_role" "lambda_stack_termination_assume_role" {
  name = "lambda_stack_termination_role_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_stack_termination_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

data "aws_iam_policy_document" "lambda_stack_termination_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "lambda_stack_termination_policy" {
  name        = "lambda_stack_termination_policy_${var.env}_${var.app_version}"
  description = "Policy for expiration checking Lambda, Digital Workforce"

  policy = "${data.aws_iam_policy_document.lambda_stack_termination_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_stack_termination_attach" {
  role       = "${aws_iam_role.lambda_stack_termination_assume_role.name}"
  policy_arn = "${aws_iam_policy.lambda_stack_termination_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "lambda_stack_termination_logs_attach" {
  role       = "${aws_iam_role.lambda_stack_termination_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}

data "aws_iam_policy_document" "lambda_stack_termination_permissions_policy" {
  statement {
    sid = "SSMPermissions${var.env}${var.app_version}"
    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters"
    ]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}",
      "${aws_ssm_parameter.ses_source_email_address.arn}",
      "${aws_ssm_parameter.dw_email_sqs.arn}",
      "${aws_ssm_parameter.ses_template_on_deletion_successful.arn}",
      "${aws_ssm_parameter.dw_admin_emails.arn}",
      "${aws_ssm_parameter.password_prefix.arn}",
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
  statement {
    sid = "SSMDeletePermissions${var.env}${var.app_version}"
    actions = ["ssm:DeleteParameter"]
    resources = [
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:UpdateItem",
      "dynamodb:DeleteItem"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
  statement {
    sid = "CloudformationPermissions${var.env}${var.app_version}"
    actions = [
      "cloudformation:DeleteStack"
    ]
    resources = ["*"]
  }
  statement {
    sid = "WorkspacesPermissions${var.env}${var.app_version}"
    actions = [
      "workspaces:TerminateWorkspaces",
      "workspaces:DescribeWorkspaces",
    ]
    resources = ["*"]
  }
  statement {
    sid = "SESpermissions${var.env}${var.app_version}"
    actions = [
      "ses:SendTemplatedEmail"
    ]
    resources = ["*"]
  }
}

resource "aws_lambda_permission" "lambda_stack_termination_exec_from_cloudwatch" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda_stack_termination.function_name}"
    principal = "events.amazonaws.com"
    source_arn = "${aws_cloudwatch_event_rule.stack_termination_check_every_day.arn}"
}
