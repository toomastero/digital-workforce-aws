resource "aws_iam_policy" "cloudwatch_logs_policy" {
  name        = "Terraform_CloudWatch_logs_access_${var.env}_${var.app_version}"
  description = "Policy for accessing CloudWatch logs"

  policy = "${data.aws_iam_policy_document.cloudwatch_logs_policy_document.json}"
}

data "aws_iam_policy_document" "cloudwatch_logs_policy_document" {
  statement {
    sid = "allowCloudWatchLogs${var.env}${var.app_version}"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    resources = ["*"]
  }
}
