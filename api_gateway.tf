variable "workspaces_api_end" {
  default = "workspace"
}

variable "end_date_api_end" {
  default = "end-dates"
}

variable "change_end_date_api_end" {
  default = "end-date"
}


// RESOURCES

resource "aws_api_gateway_rest_api" "gateway_rest_api" {
  name        = "Lambda_api_gateway_${var.env}_${var.app_version}"
  description = "Api gateway for Lambda functions"

  policy = "${data.aws_iam_policy_document.api_gateway_policy_document.json}"
}

data "aws_iam_policy_document" "api_gateway_policy_document" {
  statement {
    sid = "Api_gateway_policy_document_${var.env}_${var.app_version}"
    actions = ["execute-api:Invoke"]
    resources = ["*"]
    principals = {
      type = "*"
      identifiers = ["*"]
    }
    effect = "Allow"
    /*
    condition = {
      test = "IpAddress"
      variable = "aws:SourceIp"
      values = ["${var.whitelisted_ips}"]
    }
    */
  }
}

/*
resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id   = "${aws_api_gateway_rest_api.gateway_rest_api.root_resource_id}"
  http_method   = "ANY"
  authorization = "NONE"
}
*/

resource "aws_api_gateway_deployment" "gateway_deployment" {
  depends_on = [
    "aws_api_gateway_integration.lambda_fetch_end_dates_gateway_integration",
    #"aws_api_gateway_integration.lambda_fetch_end_dates_gateway_integration_root",
    "aws_api_gateway_integration.lambda_entry_point_gateway_integration",
    #"aws_api_gateway_integration.lambda_entry_point_gateway_integration_root",
    "aws_api_gateway_integration.lambda_change_end_date_gateway_integration"
  ]

  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  stage_name  = "${var.api_gateway_stage_name}"
}


// LAMBDA_FETCH_END_DATES STUFF

resource "aws_api_gateway_resource" "lambda_fetch_end_dates_proxy_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.gateway_rest_api.root_resource_id}"
  path_part   = "${var.end_date_api_end}"
}


resource "aws_api_gateway_method" "lambda_fetch_end_dates_proxy_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id   = "${aws_api_gateway_resource.lambda_fetch_end_dates_proxy_resource.id}"
  http_method   = "GET"
  authorization = "NONE"
}


resource "aws_api_gateway_integration" "lambda_fetch_end_dates_gateway_integration" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id = "${aws_api_gateway_resource.lambda_fetch_end_dates_proxy_resource.id}"
  http_method = "${aws_api_gateway_method.lambda_fetch_end_dates_proxy_method.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.lambda_fetch_end_dates.invoke_arn}"
}

/*
resource "aws_api_gateway_integration" "lambda_fetch_end_dates_gateway_integration_root" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id = "${aws_api_gateway_method.proxy_root.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"

  integration_http_method = "GET"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.lambda_fetch_end_dates.invoke_arn}"
}
*/

resource "aws_lambda_permission" "lambda_fetch_end_dates_apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_fetch_end_dates.arn}"
  principal     = "apigateway.amazonaws.com"

  #source_arn = "${aws_api_gateway_deployment.example.execution_arn}/*/*"
  source_arn = "${aws_api_gateway_rest_api.gateway_rest_api.execution_arn}/*/*/*"
  #source_arn = "${aws_api_gateway_rest_api.gateway_rest_api.execution_arn}/*/*"
}


// LAMBDA_ENTRY_POINT STUFF

resource "aws_api_gateway_resource" "Lambda_entry_point_proxy_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.gateway_rest_api.root_resource_id}"
  path_part   = "${var.workspaces_api_end}"
}


resource "aws_api_gateway_method" "Lambda_entry_point_proxy_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id   = "${aws_api_gateway_resource.Lambda_entry_point_proxy_resource.id}"
  http_method   = "POST"
  authorization = "NONE"
}



resource "aws_api_gateway_integration" "lambda_entry_point_gateway_integration" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id = "${aws_api_gateway_resource.Lambda_entry_point_proxy_resource.id}"
  http_method = "${aws_api_gateway_method.Lambda_entry_point_proxy_method.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.Lambda_entry_point.invoke_arn}"
}

/*
resource "aws_api_gateway_integration" "lambda_entry_point_gateway_integration_root" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id = "${aws_api_gateway_method.Lambda_entry_point_proxy_method.resource_id}"
  http_method = "${aws_api_gateway_method.Lambda_entry_point_proxy_method.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.Lambda_entry_point.invoke_arn}"
}
*/

resource "aws_lambda_permission" "Lambda_entry_point_apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.Lambda_entry_point.arn}"
  principal     = "apigateway.amazonaws.com"

  #source_arn = "${aws_api_gateway_deployment.example.execution_arn}/*/*"
  source_arn = "${aws_api_gateway_rest_api.gateway_rest_api.execution_arn}/*/*/*"
  #source_arn = "${aws_api_gateway_rest_api.gateway_rest_api.execution_arn}/*/*"
}


// LAMBDA_CHANGE_END_DATE STUFF

resource "aws_api_gateway_resource" "lambda_change_end_date_proxy_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.gateway_rest_api.root_resource_id}"
  path_part   = "${var.change_end_date_api_end}"
}


resource "aws_api_gateway_method" "lambda_change_end_date_proxy_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id   = "${aws_api_gateway_resource.lambda_change_end_date_proxy_resource.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_change_end_date_gateway_integration" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id = "${aws_api_gateway_resource.lambda_change_end_date_proxy_resource.id}"
  http_method = "${aws_api_gateway_method.lambda_change_end_date_proxy_method.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.lambda_change_end_date.invoke_arn}"
}

/*
resource "aws_api_gateway_integration" "lambda_change_end_date_gateway_integration_root" {
  rest_api_id = "${aws_api_gateway_rest_api.gateway_rest_api.id}"
  resource_id = "${aws_api_gateway_method.lambda_change_end_date_proxy_method.resource_id}"
  http_method = "${aws_api_gateway_method.lambda_change_end_date_proxy_method.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.lambda_change_end_date.invoke_arn}"
}
*/

resource "aws_lambda_permission" "lambda_change_end_date_apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_change_end_date.arn}"
  principal     = "apigateway.amazonaws.com"

  #source_arn = "${aws_api_gateway_deployment.example.execution_arn}/*/*"
  source_arn = "${aws_api_gateway_rest_api.gateway_rest_api.execution_arn}/*/*/*"
  #source_arn = "${aws_api_gateway_rest_api.gateway_rest_api.execution_arn}/*/*"
}
