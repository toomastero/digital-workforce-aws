resource "aws_sns_topic" "cloudformation_workspaces_sns_topic" {
  name = "terraform_cloudformation_workspaces_sns_topic_${var.env}_${var.app_version}"
}

resource "aws_sns_topic_subscription" "user_updates_sns_target" {
  topic_arn  = "${aws_sns_topic.cloudformation_workspaces_sns_topic.arn}"
  protocol   = "lambda"
  endpoint   = "${aws_lambda_function.cf_sns_lambda.arn}"
}
