
resource "aws_lambda_function" "Lambda_entry_point" {
  function_name    = "Lambda_entry_point_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "lambda_entry_point.lambda_handler"
  runtime          = "python3.6"
  timeout          = 60
  role             = "${aws_iam_role.Lambda_entry_point_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

/*
*   IAM
*/

data "aws_iam_policy_document" "Lambda_entry_point_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "Lambda_entry_point_permissions_policy" {
  statement {
    sid = "CloudformationAndWorkspacesPermissions${var.env}${var.app_version}"
    actions = [
      "cloudformation:CreateStack",
      "cloudformation:UpdateStack",
      "cloudformation:DescribeStacks",
      "workspaces:CreateWorkspaces",
      "workspaces:DescribeWorkspaces",
      "workspaces:TerminateWorkspaces"
    ]
    resources = ["*"]
  }
  statement {
    sid = "S3TemplateFetchPermissions${var.env}${var.app_version}"
    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]
    resources = [
      "${aws_s3_bucket.lambdas_bucket.arn}/*",
      "${aws_s3_bucket.lambdas_bucket.arn}"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
      "dynamodb:UpdateItem"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
  statement {
    sid = "SNSTopicPermissions${var.env}${var.app_version}"
    actions = ["SNS:Publish"]
    resources = [
      "${aws_sns_topic.cloudformation_workspaces_sns_topic.arn}"
    ]
  }
  statement {
    sid = "SSMGetPermissions${var.env}${var.app_version}"
    actions = ["ssm:GetParameters", "ssm:GetParameter"]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}",
      "${aws_ssm_parameter.password_prefix.arn}",
      "${aws_ssm_parameter.dw_cloudformation_sns_topic.arn}",
      "${aws_ssm_parameter.dw_cloudformation_template_name.arn}",
      "${aws_ssm_parameter.dw_cloudformation_template_bucket_name.arn}",
      "${aws_ssm_parameter.secret_key.arn}"
    ]
  }
  statement {
    sid = "SSMPutPermissions${var.env}${var.app_version}"
    actions = ["ssm:PutParameter"]
    resources = ["*"]
  }
}

resource "aws_iam_role" "Lambda_entry_point_assume_role" {
  name = "Lambda_entry_point_example_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.Lambda_entry_point_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

resource "aws_iam_policy" "Lambda_entry_point_policy" {
  name        = "Lambda_entry_point_policy_${var.env}_${var.app_version}"
  description = "Policy for Lambda 1, Digital Workforce"

  policy = "${data.aws_iam_policy_document.Lambda_entry_point_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "Lambda_entry_point_attach" {
  role       = "${aws_iam_role.Lambda_entry_point_assume_role.name}"
  policy_arn = "${aws_iam_policy.Lambda_entry_point_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "Lambda_entry_point_logs_attach" {
  role       = "${aws_iam_role.Lambda_entry_point_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}
