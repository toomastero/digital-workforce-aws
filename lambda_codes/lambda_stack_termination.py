from typing import Dict, List

import logging
import sys

import boto3  # type: ignore
from lib.dynamodb_helper import DynamoDBHelper

from lib.lambda_types import LambdaDict, LambdaContext
from lib.utility_helpers import getRemainingDays
from lib.sns_helper import SnsLambdaHelper


logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

client = boto3.client('cloudformation')


def lambda_handler(event: LambdaDict, context: LambdaContext) -> Dict:
    fieldToScanList: str = ",".join([
        "useremail", "fullname", "endDate", "shoudTerminate", "stackName"
    ])
    dbHelper: DynamoDBHelper = DynamoDBHelper("")
    dynamoDbTable: str = dbHelper.dynamoDbTable
    items: List[Dict[str, str]] = dbHelper.getItems(fieldToScanList)
    itemsToBeTerminated: List = list(filter(shouldTerminate, items))

    print("itemsToBeTerminated:")
    print(itemsToBeTerminated)

    for item in itemsToBeTerminated:
        itemDbHelper = DynamoDBHelper(item["useremail"], dynamoDbTable)
        logger.info(" ***** Terminating stack with useremail: %s",
                    item["useremail"])
        if "stackName" in item:
            updateShouldTerminateField(itemDbHelper)
            snsHelper = SnsLambdaHelper(item["useremail"])
            client.delete_stack(StackName=item["stackName"])
            snsHelper.executeOnStatus("DELETE_COMPLETE")
            logger.info(" ***** Cloudformation stack deletion executed")
        else:
            updateShouldTerminateField(itemDbHelper)
            logger.error(" ***** Email %s does not have stack name. Possible error?", item["useremail"])  # pylint: disable=C0301 # noqa
    return {
        "statusCode": 200,
        "body": "Success!"
    }


def shouldTerminate(item: Dict[str, str]) -> bool:
    if "shoudTerminate" in item and item["shoudTerminate"] is True:
        return True
    # Terminate only on the next day just in case because of time difference
    if (getRemainingDays(item) + 1) <= 0:
        return True
    return False


def updateShouldTerminateField(dbHelper: DynamoDBHelper) -> None:
    dbHelper.updateFieldValue(field="shouldTerminate",
                              value=True,
                              valueType="BOOL",
                              withDatabaseLock=True)
