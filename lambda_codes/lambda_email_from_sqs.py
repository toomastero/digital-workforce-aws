from typing import Dict

import json
import logging
import sys

from lib.dynamodb_helper import DynamoDBHelper
from lib.models import WorkspaceDynamoDBItem
import lib.email_helper as EmailHelper

from lib.lambda_types import LambdaDict, LambdaContext
from lib.smm_helper import SmmHelper


logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event: LambdaDict, context: LambdaContext) -> Dict:
    for record in event["Records"]:
        body = record["body"].replace("\'", "\"")
        logger.info(" ***** Got body: %s", body)
        jsonBody = json.loads(body)
        helper = LambdaHelper(jsonBody["useremail"], int(jsonBody["daysLeft"]))
        helper.sendEmailAndUpdateEntity()

    return {
        "statusCode": 200,
        "body": "Success!"
    }


class LambdaHelper:
    dbItem: WorkspaceDynamoDBItem
    dynamoDbTable: str
    useremail: str
    daysLeft: int

    def __init__(self, useremail: str, daysLeft: int):
        self.daysLeft = daysLeft
        self.useremail = useremail
        self.dynamoDbHelper = DynamoDBHelper(useremail)
        self.dynamoDbTable = self.dynamoDbHelper.dynamoDbTable

    def sendEmailAndUpdateEntity(self) -> None:
        def wrapper() -> None:
            self.dbItem = self.dynamoDbHelper.initFromDatabase()
            if self.isEmailSent():
                logger.info(" ***** Email is already sent, email: %s",
                            self.useremail)
            else:
                emailTemplateName: str = SmmHelper.getEmailTemplateArnFromSmm(
                    "expiration_days_left")
                logger.info(" ***** Email template: %s", emailTemplateName)
                setattr(self.dbItem, "daysLeft", self.daysLeft)
                EmailHelper.sendEmail([self.useremail],
                                      self.dbItem,
                                      emailTemplateName)
                self.updateField()
        self.dynamoDbHelper.executeWithDatabaseLock(wrapper)

    def isEmailSent(self) -> bool:
        logger.info("Checking if email is already sent")
        try:
            attr = getattr(self.dbItem, "notificationsSentByDays")
            if str(self.daysLeft) in attr.split(","):
                return True
        except AttributeError:
            return False
        return False

    def updateField(self) -> None:
        attr: str
        fieldExists = False
        try:
            attr = getattr(self.dbItem, "notificationsSentByDays")
            fieldExists = True
        except AttributeError:
            fieldExists = False
        if fieldExists:
            attr = attr + ",%s" % str(self.daysLeft)
            self.dynamoDbHelper.updateFieldValue("notificationsSentByDays",
                                                 attr)
        else:
            self.dynamoDbHelper.updateFieldValue("notificationsSentByDays",
                                                 str(self.daysLeft))
