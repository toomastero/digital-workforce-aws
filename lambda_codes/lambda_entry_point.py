from typing import Dict, List

import random
import logging
import re
import boto3  # type: ignore

from lib.lambda_types import LambdaDict, LambdaContext
from lib.dynamodb_helper import DynamoDBHelper
from lib.smm_helper import SmmHelper
from lib.utility_helpers import filterBodyToDict

logger = logging.getLogger()
logger.setLevel(logging.INFO)

"""
This Lambda should start CloudFormation stack.
For testing these values can be used:
 - username: username1 (toomas.tero)
 - useremail: user1@example.com (toomas.tero@nordcloud.com)
 - directoryId: d-996734a395
 - bundleId: wsb-bh8rsxt14
"""
paramList: List[str] = ["username", "useremail", "directoryId",
                        "bundleId", "fullname", "startDate",
                        "endDate", "password", "secretKey"]

cloudFormationBucketName: str
cloudformationTemplateName: str
cloudFormationSNSTopic: str
dynamoDbTable: str


def lambda_handler(event: LambdaDict, context: LambdaContext) -> LambdaDict:

    if "body" not in event:
        logger.info("Error: No attributes passed in the body")
        return returnHtml(400, "Error: No attributes passed in the body")

    params: Dict = filterBodyToDict(event["body"])

    if not checkParameters(params):
        missingParams = [v for v in paramList if v not in params]
        logger.info("Error: All parameters should exist")
        return returnHtml(
            400,
            "Error: All parameters should exist. Missing: %s" % missingParams
        )

    if not SmmHelper.secretKeyIsValid(params["secretKey"]):
        return returnHtml(400, "Secret key is invalid")

    params["useremail"] = str(params["useremail"]).strip()
    smmError: Dict = getParametersFromSystemManager()
    if smmError["error"]:
        return returnHtml(500, smmError["message"])

    dbHelper = DynamoDBHelper(params["useremail"], dynamoDbTable)
    dbExistingError: Dict = dbHelper.checkIfUserExists()
    if dbExistingError["error"]:
        return returnHtml(500, dbExistingError["message"])

    cfError: Dict = startCloudFormationTemplate(params)
    if cfError["error"]:
        return returnHtml(500, cfError["message"])
    params["stackName"] = cfError["stackName"]

    dbError: Dict = dbHelper.putTestValue(params)
    if dbError["error"]:
        return returnHtml(500, dbError["message"])

    createPasswordSsmValue(params)

    logger.info(" ***** Execution successful")
    return returnHtml(200, generateSuccessfulReturnMessage(params))


def getParametersFromSystemManager() -> Dict:
    global dynamoDbTable, cloudFormationSNSTopic, cloudformationTemplateName, cloudFormationBucketName  # pylint: disable=C0301,W0603 # noqa

    response = SmmHelper.getParameters([
        "dw_dynamodb_table",
        "dw_cloudformation_sns_topic",
        "dw_cloudformation_template_name",
        "dw_cloudformation_template_bucket_name"
    ])

    logger.info(" ***** Got parameters from AWS System Manager:")
    logger.info(" ***** %s", response)
    logger.info(" ***** Starting to iterate keys")
    for obj in response:
        logger.info(" ***** Name: %s, Value: %s", obj["Name"], obj["Value"])
        if obj["Name"] == "dw_dynamodb_table":
            dynamoDbTable = obj["Value"]
        elif obj["Name"] == "dw_cloudformation_sns_topic":
            cloudFormationSNSTopic = obj["Value"]
        elif obj["Name"] == "dw_cloudformation_template_name":
            cloudformationTemplateName = obj["Value"]
        elif obj["Name"] == "dw_cloudformation_template_bucket_name":
            cloudFormationBucketName = obj["Value"]

    if (dynamoDbTable and cloudFormationSNSTopic and
            cloudformationTemplateName and cloudFormationBucketName):
        return {"error": False}

    return {"error": True, "message": "Not all needed parameters were set " +
                                      "from AWS System Manager"}


def checkParameters(paramsDict: Dict) -> bool:
    for param in paramList:
        if param not in paramsDict:
            return False
    return True


def startCloudFormationTemplate(params: Dict) -> Dict:
    client = boto3.client('cloudformation')
    logger.info("***** Creating a new CloudFormation stack")

    logger.info("***** Trying to get CF template file")
    s3 = boto3.resource('s3')
    templateFile = s3.Object(cloudFormationBucketName,
                             cloudformationTemplateName)
    template = templateFile.get()["Body"].read().decode('utf-8')
    logger.info("***** Got CF template file: %s", template)
    logger.info("***** type of CF template file: %s", type(template))
    legalUserName = re.sub('[^A-Za-z0-9-]+', '', params["username"])
    stackName = "workspace-stack-{}-{}".format(
        legalUserName,
        random.randint(1, 100)
    )

    try:
        client.create_stack(
            StackName=stackName,
            TemplateBody=template,
            OnFailure="DELETE",
            Parameters=[
                {
                    'ParameterKey': "BundleId",
                    'ParameterValue': params["bundleId"]
                },
                {
                    'ParameterKey': "DirectoryId",
                    'ParameterValue': params["directoryId"]
                },
                {
                    'ParameterKey': "UserName",
                    'ParameterValue': params["username"]
                },
                {
                    'ParameterKey': "UserEmail",
                    'ParameterValue': params["useremail"]
                },
                {
                    'ParameterKey': "StartDate",
                    'ParameterValue': params["startDate"]
                },
                {
                    'ParameterKey': "EndDate",
                    'ParameterValue': params["endDate"]
                }
            ],
            NotificationARNs=[cloudFormationSNSTopic]
        )
        logger.info("***** New CloudFormation stack creating is triggered")
        return {"error": False, "stackName": stackName}

    except Exception as exc:
        logger.error("Error: cannot create a CloudFormation stack")
        logger.error("Error: %s", exc)
        return {
            "error": True,
            "message":
                "Error: cannot create a CloudFormation stack: {}".format(exc)
        }


def returnHtml(statusCode: int, message: str) -> Dict:
    return {
        "statusCode": statusCode,
        "headers": {
            "Content-Type": "text/html; charset=utf-8",
        },
        "body": "{}".format(message)
    }


def generateSuccessfulReturnMessage(params: Dict) -> str:
    return "".join([
        "Successfully starter a new stack creation ",
        "for an user with email: {}".format(params["useremail"])
    ])


def createPasswordSsmValue(params: Dict) -> None:
    username: str = params["username"]
    password: str = params["password"]

    SmmHelper.putPasswordField(username, password)
