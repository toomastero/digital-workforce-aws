from typing import Dict, List, Optional, NamedTuple

import logging
import sys

import boto3  # type: ignore
from lib.dynamodb_helper import DynamoDBHelper
from lib.smm_helper import SmmHelper
from lib.models import WorkspaceDynamoDBItem
from lib.lambda_types import LambdaDict, LambdaContext
from lib.utility_helpers import filterBodyToDict
from lib.workspaces_helper import getWorkspaceIdByEmailAndDates
from lib.email_helper import sendEmail


logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class PostData(NamedTuple):
    useremail: str
    username: str
    newEndDate: str
    oldEndDate: str
    startDate: str
    sendToAdmins: bool
    sendToClient: bool
    secretKey: str


def lambda_handler(event: LambdaDict, context: LambdaContext) -> Dict:
    logger.info(" ***** Got event:")
    logger.info(" ***** %s", event)

    if "body" not in event:
        return returnMsg(400, "POST values missing")

    postData = getFilteredPostData(event["body"])
    if not postData:
        return returnMsg(400, "Post data values are missing")
    assert postData is not None

    if not SmmHelper.secretKeyIsValid(postData.secretKey):
        return returnMsg(400, "Secret key is invalid")

    logger.info(" ***** Got email: %s", postData.useremail)

    logger.info(" ***** Trying to update Workspace tag")
    updateTag(postData)
    logger.info(" ***** Trying to update DB field endDate and send email")
    updateDbAndSendEmails(postData)

    return returnMsg(200, "End date updated to %s for %s" %
                     (postData.newEndDate, postData.useremail))


def updateDbAndSendEmails(postData: PostData) -> None:
    dbHelper: DynamoDBHelper = DynamoDBHelper(postData.useremail)
    dbHelper.updateFieldValue(field="endDate",
                              value=postData.newEndDate,
                              withDatabaseLock=True)
    item: WorkspaceDynamoDBItem = dbHelper.initFromDatabase()

    # Get stuff from SMM
    adminEmails: str = SmmHelper.getParameter("dw_admin_emails")
    emails: List = adminEmails.replace(" ", "").split(",")
    emailTemplateForAdmins: str = SmmHelper.getEmailTemplateArnFromSmm(
        "end_date_updated_admins")
    emailTemplateForUser: str = SmmHelper.getEmailTemplateArnFromSmm(
        "end_date_updated_user")

    # Send emails
    if postData.sendToAdmins:
        logger.info(" ***** Sending emails to admins")
        sendEmail(emails,
                  item,
                  emailTemplateForAdmins)
    if postData.sendToClient:
        logger.info(" ***** Sending emails to the client")
        sendEmail([postData.useremail],
                  item,
                  emailTemplateForUser)


def updateTag(postData: PostData) -> None:
    workspaceId = getWorkspaceIdByEmailAndDates(
        postData.useremail,
        postData.username,
        postData.startDate,
        postData.oldEndDate,
    )
    if not workspaceId:
        raise Exception(" ***** No all parameters specified" +
                        "or workspace found for email: %s ",
                        postData.useremail)
    client = boto3.client('workspaces')
    client.create_tags(
        ResourceId=workspaceId,
        Tags=[{"Key": "endDate", "Value": postData.newEndDate}])


def getFilteredPostData(bodyRawInput: str) -> Optional[PostData]:
    filtered: Dict = filterBodyToDict(bodyRawInput)
    sendToAdmins: bool = (True if (  # pylint: disable=R1719
        "sendToAdmins" in filtered and isTrue(filtered["sendToAdmins"]))
        else False)  # pylint: disable=C0330
    sendToClient: bool = (True if (  # pylint: disable=R1719
        "sendToClient" in filtered and isTrue(filtered["sendToClient"]))
        else False)  # pylint: disable=C0330

    toTestList: List[str] = ["useremail", "username", "newEndDate",
                             "oldEndDate", "startDate", "secretKey"]
    sameKeys: List[str] = list(set(toTestList) & set(filtered.keys()))

    if len(sameKeys) == len(toTestList):
        return PostData(
            filtered["useremail"],
            filtered["username"],
            filtered["newEndDate"],
            filtered["oldEndDate"],
            filtered["startDate"],
            sendToAdmins,
            sendToClient,
            filtered["secretKey"]
        )
    return None


def returnMsg(statusCode: int, message: str) -> Dict:
    return {
        "statusCode": statusCode,
        "headers": {
            "Content-Type": "text/html; charset=utf-8",
        },
        "body": message
    }


def isTrue(param: str) -> bool:
    return param == "True" or param.lower() == "true"
