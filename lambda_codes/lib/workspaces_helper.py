from typing import Dict, List, Optional, NamedTuple

import logging
import sys

import boto3  # type: ignore

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class TagsDict(NamedTuple):
    key: str
    value: str


def getWorkspaceIdByEmailAndDates(useremail: str,
                                  username: str,
                                  startDate: str,
                                  oldEndDate: str) -> Optional[str]:
    if (not useremail or not username or not startDate or not oldEndDate):
        return None
    client = boto3.client('workspaces')
    resp: Dict = client.describe_workspaces()
    workspaces: List = resp["Workspaces"]

    while "NextToken" in resp:
        resp = client.describe_workspaces(NextToken=resp["NextToken"])
        if "Workspaces" in resp:
            workspaces.extend(resp["Workspaces"])

    if workspaces:
        workspaceIds: List[str] = list(map(
            lambda w: w["WorkspaceId"],
            (w for w in workspaces if filterWorkspace(w, username))))
        for workspaceId in workspaceIds:
            tagsResp: Dict = client.describe_tags(ResourceId=workspaceId)
            if "TagList" in tagsResp:
                tagList: List[TagsDict] = list(map(
                    lambda t: TagsDict(t["Key"], t["Value"]),
                    tagsResp["TagList"]))
                if filterWorkspaceByTags(tagList,
                                         useremail,
                                         startDate,
                                         oldEndDate):
                    return workspaceId
    return None


def filterWorkspaceByTags(tagList: List[TagsDict],
                          useremail: str,
                          startDate: str,
                          oldEndDate: str) -> bool:
    emailDict: Optional[TagsDict] = find(tagList, "userEmail")
    startDateDict: Optional[TagsDict] = find(tagList, "startDate")
    endDateDict: Optional[TagsDict] = find(tagList, "endDate")

    if (emailDict and startDateDict and endDateDict):
        return (emailDict.value == useremail and
                startDateDict.value == startDate and
                endDateDict.value == oldEndDate)
    return False


def find(tagList: List[TagsDict], keyName: str) -> Optional[TagsDict]:
    return next((x for x in tagList if x.key == keyName), None)


def filterWorkspace(workspace: Dict, username: str) -> bool:
    return ("WorkspaceId" in workspace and
            "UserName" in workspace and
            workspace["UserName"] == username)
