from typing import List

import logging
import sys
from copy import deepcopy
import boto3  # type: ignore

from .models import WorkspaceDynamoDBItem
from .dynamodb_helper import DynamoDBHelper
from .email_helper import sendEmail
from .smm_helper import SmmHelper

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class SnsLambdaHelper:
    useremail: str
    dbItem: WorkspaceDynamoDBItem
    dbHelper: DynamoDBHelper

    def __init__(self, useremail: str):
        self.useremail = useremail
        self.dbHelper = DynamoDBHelper(useremail)
        self.dbItem = self.dbHelper.initFromDatabase()
        if self.dbItem is None:
            raise Exception("dbItem is None, cannot continue")

    def executeOnStatus(self, resourceStatus: str) -> str:
        if resourceStatus == "CREATE_FAILED":
            return self.onCreateFailed()
        if resourceStatus == "CREATE_COMPLETE":
            return self.onCreateComplete()
        if resourceStatus == "DELETE_COMPLETE":
            return self.onDeleteComplete()

        logger.info(" ***** None of resource statuses matched")
        return "Execution finished"

    def updateNotificationField(self,
                                fieldName: str,
                                fieldValue: bool) -> None:
        return self.dbHelper.updateFieldValue(fieldName, fieldValue, "BOOL")

    def onCreateComplete(self) -> str:
        logger.info(" ***** Inside CREATE_COMPLETE")
        self.sendEmailAndUpdateField("on_creation_successful",
                                     "createNotificationSent")
        self.sendEmailWithPassword()
        return "Inside CREATE_COMPLETE, finished"

    def onCreateFailed(self) -> str:
        logger.info(" ***** Inside CREATE_FAILED")
        self.sendEmailAndUpdateField("on_creation_failed",
                                     "failedNotificationSent")

        self.dbHelper.removeFromDynamoDB()
        return "Inside CREATE_FAILED, finished"

    def onDeleteComplete(self) -> str:
        logger.info(" ***** Inside DELETE_COMPLETE")
        self.sendEmailAndUpdateField("on_deletion_successful",
                                     "removeNotificationSent",
                                     True)

        self.dbHelper.removeFromDynamoDB()

        passwordPrefix: str = SmmHelper.getParameter("password_prefix")
        passwordSsmName: str = "{}_{}".format(passwordPrefix,
                                              self.dbItem.username)
        SmmHelper.deleteParameter(passwordSsmName)
        return "Inside DELETE_COMPLETE, finished"

    def sendEmailAndUpdateField(self, actionName: str,
                                fieldName: str,
                                sendToUser: bool = False) -> None:
        def wrapper() -> None:
            if getattr(self.dbItem, fieldName) is True:
                logger.info(" ***** Email is already sent, fieldName: %s",
                            fieldName)
                return

            emailTemplateName: str = SmmHelper.getEmailTemplateArnFromSmm(
                actionName)
            adminEmails: str = SmmHelper.getParameter("dw_admin_emails")
            emails: List = adminEmails.replace(" ", "").split(",")
            if sendToUser:
                emails.append(self.useremail)

            logger.info("".join([" ***** Sending an email with template ",
                                 "%s " % emailTemplateName,
                                 "to these addresses: %s" % str(emails)]))

            emailResponse = sendEmail(emails,
                                      self.dbItem,
                                      emailTemplateName)
            logger.info(" ***** Email response:")
            logger.info("%s", emailResponse)
            self.updateNotificationField(fieldName, True)
        self.dbHelper.executeWithDatabaseLock(wrapper)

    def sendEmailWithPassword(self) -> None:
        logger.info(" ***** In sendEmailWithPassword function")
        emailTemplateName: str = SmmHelper.getEmailTemplateArnFromSmm(
            "on_creation_successful_email_to_client")
        registrationCode: str = getRegistrationCode(self.dbItem.directoryId)
        passwordPrefix: str = SmmHelper.getParameter("password_prefix")
        passwordSsmName: str = "{}_{}".format(passwordPrefix,
                                              self.dbItem.username)
        email: str = self.dbItem.useremail
        username: str = self.dbItem.username
        password: str = SmmHelper.getParameter(passwordSsmName)

        logger.info(" ***** Checking if all parameters fetched")

        if len(list(i for i in [registrationCode, username,
                                email, password] if i)) != 4:
            raise Exception("Could not get the needed variables. " +
                            "Cannot continue. Email: %s" % email)

        dbItem = deepcopy(self.dbItem)
        setattr(dbItem, "password", password)
        setattr(dbItem, "registrationCode", registrationCode)

        sendEmail([email],
                  dbItem,
                  emailTemplateName)


def getRegistrationCode(directoryId: str) -> str:
    client = boto3.client('workspaces')
    resp = client.describe_workspace_directories(DirectoryIds=[directoryId])
    if "Directories" in resp:
        return resp["Directories"][0]["RegistrationCode"]
    raise Exception("Registration code for directory id %s is empty. Cannot continue" % directoryId)  # pylint: disable=C0301 # noqa
