from typing import Dict

import logging
import sys
import datetime
import json
from urllib.parse import parse_qsl
from dateutil import parser


logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def getRemainingDays(item: Dict) -> int:
    useremail: str = item["useremail"]
    logger.info(" ***** Trying to get remaining days for item: %s", useremail)
    if "endDate" not in item:
        logger.info(" ***** Spotted an item without endDate: %s", useremail)
        return -1
    now: int = int(datetime.datetime.now().strftime("%j"))
    endDay: int = int(parser.parse(item["endDate"]).strftime("%j"))
    daysLeft: int = endDay - now
    logger.info(" ***** Days left for this user %s", daysLeft)
    return daysLeft


def isStartDateInPast(item: Dict) -> bool:
    if "startDate" in item:
        return parser.parse(item["startDate"]) <= datetime.datetime.now()

    logger.info(" ***** startDate is not found for: %s", item["useremail"])
    return False


def filterBodyToDict(body: str) -> Dict:
    res: Dict
    try:
        res = json.loads(body)
    except ValueError:
        res = dict(parse_qsl(body))
    return res
