from typing import Dict, Callable, Any, List, Optional

import datetime
import time
import logging
import boto3  # type: ignore

from .models import WorkspaceDynamoDBItem
from .smm_helper import SmmHelper

logger = logging.getLogger()
logger.setLevel(logging.INFO)

LOCK_TIMEOUT_SEC: int = 5


class DynamoDBHelper:
    useremail: str
    dynamoDbTable: str
    client: Any

    def __init__(self,
                 useremail: str,
                 dynamoDbTable: Optional[str] = None) -> None:
        self.client = boto3.client('dynamodb')
        self.useremail = useremail
        if dynamoDbTable:
            self.dynamoDbTable = dynamoDbTable
        else:
            self.dynamoDbTable = SmmHelper.getDynamoDBTableName()

    def checkIfUserExists(self) -> Dict:
        logger.info("***** Trying to check if email exists in the DynamoDB")

        try:
            response = self.client.get_item(
                TableName=self.dynamoDbTable,
                Key={"useremail": {"S": self.useremail}},
                ConsistentRead=True
            )
            logger.info("***** Got a response from DynamoDB:")
            logger.info("***** %s", response)

            if "Item" in response:
                return {"error": True,
                        "message": "Item is already in a database"}
            return {"error": False}
        except Exception as exc:
            return {
                "error": True,
                "message": "Error: could not read an item from DynamoDB. " +
                           "Stacktrace: {}".format(exc)
            }

    def putTestValue(self, params: Dict) -> Dict:
        timestamp: str = str(datetime.datetime.now())

        mappedParams: Dict = {}
        for key, value in params.items():
            mappedParams[key] = {"S": value}

        itemDict: Dict = {
            "createdAt": {"S": "{}".format(timestamp)},
            "updatedAt": {"S": "{}".format(timestamp)},
            "lockTimestamp": {"N": str(0)},
            "notificationsSentByDays": {"S": "0"},
            "shoudTerminate": {"BOOL": False},
            "createNotificationSent": {"BOOL": False},
            "failedNotificationSent": {"BOOL": False},
            "removeNotificationSent": {"BOOL": False}
        }
        try:
            item: Dict = {**itemDict, **mappedParams}
            logger.info("***** Trying to put item into DynamoDB")
            logger.info("***** Item: %s", item)
            self.client.put_item(TableName=self.dynamoDbTable, Item=item)
            logger.info("***** Putting an item into DynamoDB successful")
            return {"error": False}
        except Exception as exc:
            logger.error("***** Error: could not put an item into DynamoDB")
            logger.error("***** Error: %s", exc)
            return {
                "error": True,
                "message": "Error: could not put an item into DynamoDB. " +
                           "Stacktrace: {}".format(exc)
            }

    def initFromDatabase(self) -> WorkspaceDynamoDBItem:
        response = self.client.get_item(
            TableName=self.dynamoDbTable,
            Key={"useremail": {"S": self.useremail}}
        )
        logger.info(" ***** Inside initFromDatabase, got response")
        logger.info("%s", response)
        if ("Item" in response and "useremail" in response["Item"]):
            item = response["Item"]
            returnValue: WorkspaceDynamoDBItem = WorkspaceDynamoDBItem()
            for keyName in WorkspaceDynamoDBItem.__annotations__.keys():  # pylint: disable=E1101
                if item[keyName]:
                    setattr(returnValue, keyName, list(
                        item[keyName].values())[0])
            return returnValue
        raise Exception("Could not get value using email {} in database {}"
                        .format(self.useremail, self.dynamoDbTable))

    def lockDynamoDB(self, iteration: int = 0) -> bool:
        if iteration == 10:
            raise Exception("DynamoDB has been locked for more than 10 seconds. " +
                            "Email used: {}".format(self.useremail))
        if self.isDbLocked():
            time.sleep(1)
            return self.lockDynamoDB(iteration + 1)

        logger.info(" ***** Locking the item: %s", self.useremail)
        sec = str(int(time.time()))  # convert to full integers
        self.updateFieldValue("lockTimestamp", str(sec), "N")
        return True

    def unlockDynamoDB(self) -> None:
        logger.info(" ***** Unlocking the item %s", self.useremail)
        self.updateFieldValue("lockTimestamp", "0", "N")

    def updateFieldValue(self,
                         field: str,
                         value: Any,
                         valueType: str = "S",
                         withDatabaseLock: bool = False) -> None:
        logger.info(" ***** Updating the field %s", field)

        def wrapper() -> None:
            self.client.update_item(
                TableName=self.dynamoDbTable,
                Key={"useremail": {"S": self.useremail}},
                UpdateExpression="SET %s = :val1" % field,
                ConditionExpression="attribute_exists(useremail)",
                ExpressionAttributeValues={':val1': {"%s" % valueType: value}}
            )
        if withDatabaseLock:
            self.executeWithDatabaseLock(wrapper)
        else:
            wrapper()

    def isDbLocked(self) -> bool:
        logger.info(" ***** Checking if the item in the database is locked")
        response = self.client.get_item(
            TableName=self.dynamoDbTable,
            Key={"useremail": {"S": self.useremail}}
        )
        if "Item" in response and self.useremail in response["Item"]:
            item: Dict = response["Item"][self.useremail]
            currentTimestamp: int = int(time.time())
            if ("lockTimestamp" in item and
                    (int(item["lockTimestamp"]) + LOCK_TIMEOUT_SEC) > currentTimestamp):
                logger.info(" ***** Item is locked")
                return True
        logger.info(" ***** Item is NOT locked")
        return False

    def executeWithDatabaseLock(self, func: Callable) -> Callable:
        logger.info("***** Executing executeWithDatabaseLock with useremail "
                    " + %s and dynamoDbTable %s",
                    self.useremail,
                    self.dynamoDbTable)  # pylint: disable=C0301

        self.lockDynamoDB()
        funcValue = func()
        self.unlockDynamoDB()
        return funcValue

    def removeFromDynamoDB(self) -> bool:
        logger.info(" ***** Deleting an item from DB, email: %s",
                    self.useremail)
        response = self.client.delete_item(
            TableName=self.dynamoDbTable,
            Key={"useremail": {"S": self.useremail}}
        )
        return "Attributes" in response

    def getItems(self, fieldToScanList: str) -> List[Dict[str, str]]:
        logger.info(" ***** Trying to get items in DynamoDB")
        table = boto3.resource('dynamodb').Table(self.dynamoDbTable)
        response = table.scan(Select="SPECIFIC_ATTRIBUTES",
                              ProjectionExpression=fieldToScanList)
        data: List[Dict[str, str]] = response['Items']
        while 'LastEvaluatedKey' in response:
            response = table.scan(Select="SPECIFIC_ATTRIBUTES",
                                  ProjectionExpression=fieldToScanList,
                                  ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])
        logger.info(" ***** Ready getting items")
        return data
