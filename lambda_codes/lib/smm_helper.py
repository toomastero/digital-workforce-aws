from typing import List, Dict
import logging
import sys
import boto3  # type: ignore

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class SmmHelper:
    @staticmethod
    def getEmailTemplateArnFromSmm(action: str) -> str:
        logger.info(" ***** Getting an email template from SMM")
        name: str = "ses_template_" + action
        client = boto3.client('ssm', region_name='eu-west-1')
        response = client.get_parameter(Name=name)
        if response["Parameter"]:
            arn = response["Parameter"]["Value"]
            logger.info(" ***** Template arn: %s", arn)
            return arn
        raise Exception("Could not get email template with name: {}"
                        .format(name))

    @staticmethod
    def getParameters(params: List) -> Dict:
        client = boto3.client('ssm')
        response = client.get_parameters(
            Names=params,
        )
        if "Parameters" not in response:
            return {"error": True, "message": "Could not get parameters" +
                                              "from AWS System Manager"}
        return response["Parameters"]

    @staticmethod
    def getParameter(param: str) -> str:
        client = boto3.client('ssm')
        response = client.get_parameter(Name=param, WithDecryption=True)
        obj = response["Parameter"]
        if obj["Name"] == param:
            return obj["Value"]
        raise Exception("Could not get value for the param: %s" % param)

    @staticmethod
    def getDynamoDBTableName(paramName: str = "dw_dynamodb_table") -> str:
        client = boto3.client('ssm')
        response = client.get_parameter(Name=paramName)
        if ("Parameter" not in response and
                "Value" not in response["Parameter"]):
            raise Exception(
                "Cannot get DynamoDB table from AWS System Manager")
        return response["Parameter"]["Value"]

    @staticmethod
    def putPasswordField(name: str,
                         value: str,
                         overwrite: bool = True) -> bool:
        client = boto3.client('ssm')
        passwordNamePrefix = SmmHelper.getParameter("password_prefix")
        fullName = "{}_{}".format(passwordNamePrefix, name)  # He's watching...

        client.put_parameter(Name=fullName,
                             Value=value,
                             Type="SecureString",
                             Overwrite=overwrite)

        return True

    @staticmethod
    def deleteParameter(name: str) -> None:
        client = boto3.client('ssm')
        client.delete_parameter(Name=name)

    @staticmethod
    def secretKeyIsValid(value: str) -> bool:
        logger.info(" ***** Getting a secret key from SSM")
        return SmmHelper.getParameter("secret_key") == value
