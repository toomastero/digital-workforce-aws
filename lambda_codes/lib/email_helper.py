from typing import List

import json
import logging
import boto3  # type: ignore
from .models import WorkspaceDynamoDBItem
from .smm_helper import SmmHelper


logger = logging.getLogger()
logger.setLevel(logging.INFO)

CHARSET = "utf-8"
REGION = "eu-west-1"


def sendEmail(destinationEmails: List[str],
              dbItem: WorkspaceDynamoDBItem,
              emailTemplateName: str,
              region: str = REGION) -> bool:
    client = boto3.client("ses", region_name=region)
    smmHelper: SmmHelper = SmmHelper()
    sourceEmailAddress: str = smmHelper.getParameter(
        "ses_source_email_address")

    logger.info("***** Trying to send an email")
    logger.info("***** Destinations: %s", str(destinationEmails))

    client.send_templated_email(
        Source=sourceEmailAddress,
        Destination={'ToAddresses': destinationEmails},
        Template=emailTemplateName,
        TemplateData=json.dumps(dbItem.__dict__)
    )

    logger.info("***** EMAIL IS SENT")
    return True
