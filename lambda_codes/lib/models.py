
class WorkspaceDynamoDBItem:  # pylint: disable=R0903
    useremail: str
    fullname: str
    username: str
    startDate: str
    endDate: str
    directoryId: str
    bundleId: str
    stackName: str
    createdAt: str
    updatedAt: str
    lockTimestamp: int
    createNotificationSent: bool
    failedNotificationSent: bool
    removeNotificationSent: bool
    shoudTerminate: bool
    notificationsSentByDays: str  # Expiration emails
