from typing import Dict, Optional, List

import json
import logging
import sys
import re

from lib.lambda_types import LambdaDict, LambdaContext
from lib.sns_helper import SnsLambdaHelper

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def getValue(message: str, key: str) -> str:
    regexString = r"{}='(.*?)'".format(key)
    matchObj = re.search(regexString, message)
    if not matchObj:
        exceptionMessage = "Key '%s' is not found in message '%s'" % (
            key, message)
        raise Exception(exceptionMessage)
    return matchObj.group(1)


def lambda_handler(event: LambdaDict, context: LambdaContext) -> str:
    logger.info(" ***** Got event")
    logger.info(event)

    record: Dict = event["Records"][0]

    snsObject: Dict = record["Sns"]
    message: str = snsObject["Message"]

    logger.info(" ***** Got message:")
    logger.info("\n%s", message)

    resourceStatus: str = getValue(message, "ResourceStatus")
    resourcePropertiesRaw: Optional[Dict] = json.loads(
        getValue(message, "ResourceProperties"))

    logger.info(" ***** Got resourceStatus: %s", resourceStatus)
    logger.info(" ***** Got resourceProperties: %s", resourcePropertiesRaw)
    if not resourcePropertiesRaw:
        return "resourcePropertiesRaw is empty, aborting the execution"

    useremail: str = getEmailFromResourceProperties(resourcePropertiesRaw)
    lambdaHelper: SnsLambdaHelper = SnsLambdaHelper(useremail)
    return lambdaHelper.executeOnStatus(resourceStatus)


def getEmailFromResourceProperties(resourceProperties: Optional[Dict]) -> str:
    if resourceProperties and resourceProperties["Tags"]:
        tagsList: List = resourceProperties["Tags"]
        for item in tagsList:
            if "Key" in item and item["Key"] == "userEmail":
                return item["Value"]
    raise Exception("User email is not found from the resource properties")
