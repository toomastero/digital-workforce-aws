from typing import Dict, List

import logging
import sys
import json

from lib.dynamodb_helper import DynamoDBHelper
from lib.lambda_types import LambdaDict, LambdaContext


logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event: LambdaDict, context: LambdaContext) -> Dict:
    fieldToScanList: str = ",".join(["useremail",
                                     "endDate",
                                     "startDate",
                                     "username"])
    dbHelper: DynamoDBHelper = DynamoDBHelper("")
    items: List[Dict[str, str]] = dbHelper.getItems(fieldToScanList)

    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json; charset=utf-8",
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(items)
    }
