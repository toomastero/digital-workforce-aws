from typing import Dict, List, Any

import logging
import sys

import boto3  # type: ignore
from lib.dynamodb_helper import DynamoDBHelper

from lib.lambda_types import LambdaDict, LambdaContext
from lib.smm_helper import SmmHelper
from lib.utility_helpers import getRemainingDays, isStartDateInPast


logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event: LambdaDict, context: LambdaContext) -> Dict:
    fieldToScanList: str = ",".join([
        "useremail", "fullname", "startDate",
        "endDate", "notificationsSentByDays"
    ])
    dbHelper: DynamoDBHelper = DynamoDBHelper("")
    items: List[Dict[str, str]] = dbHelper.getItems(fieldToScanList)

    notificationsDict: Dict[str, List] = {
        # Days left
        "1": [],
        "5": [],
        "10": [],
        "15": []
    }
    for key, val in notificationsDict.items():
        for item in list(filter(isStartDateInPast, items)):
            daysLeft: int = getRemainingDays(item)
            logger.info(" ***** Email: %s, daysLeft: %s, key: %s",
                        item["useremail"], daysLeft, key)
            keyInNotifications: bool = keyInNotificationsSentByDaysField(
                int(key), item)
            logger.info(" ***** keyInNotifications: %s", keyInNotifications)
            if (int(key) == daysLeft and not keyInNotifications):
                logger.info(" ***** Adding %s for sending an email, key: %s",
                            item["useremail"], key)
                val.append(item)

    logger.info(" ***** notificationsDict: %s", notificationsDict)

    sqsHelper = SqsHelper()
    for key, val in notificationsDict.items():
        for item in val:
            item["daysLeft"] = key
            logger.info(" ***** Sending an email to %s, daysLeft: %s",
                        item["useremail"], key)
            sqsHelper.sendMessage(item)

    return {
        "statusCode": 200,
        "body": "Expiration check lambda executed successfully"
    }


def keyInNotificationsSentByDaysField(key: int, item: Dict) -> bool:
    fieldExists = "notificationsSentByDays" in item
    if fieldExists:
        keyExists = str(key) in item["notificationsSentByDays"].split(",")
        return keyExists
    return False


class SqsHelper:  # pylint: disable=R0903
    queue: Any

    def __init__(self) -> None:
        sqsQueueUrl: str
        queueName = "dw_email_sqs"
        response = SmmHelper.getParameters([queueName])
        for obj in response:
            if obj["Name"] == queueName:
                sqsQueueUrl = obj["Value"]
        if not sqsQueueUrl:
            message = "Could not get SQS url using name {}".format(queueName)
            raise Exception(message)
        self.queue = boto3.resource('sqs').Queue(sqsQueueUrl)

    def sendMessage(self, jsonParams: Dict) -> bool:
        logger.info(" ***** Sending a new SQS message")
        response = self.queue.send_message(MessageBody=str(jsonParams))
        if "MessageId" in response:
            return True
        return False
