resource "aws_lambda_function" "lambda_email_from_sqs" {
  function_name    = "lambda_email_from_sqs_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "lambda_email_from_sqs.lambda_handler"
  runtime          = "python3.6"
  timeout          = 30
  role             = "${aws_iam_role.lambda_email_from_sqs_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

/*
*   IAM
*/

resource "aws_iam_role" "lambda_email_from_sqs_assume_role" {
  name = "lambda_email_from_sqs_role_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_email_from_sqs_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

data "aws_iam_policy_document" "lambda_email_from_sqs_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "lambda_email_from_sqs_policy" {
  name        = "lambda_email_from_sqs_policy_${var.env}_${var.app_version}"
  description = "Policy for Lambda sending emails from SQS, Digital Workforce"

  policy = "${data.aws_iam_policy_document.lambda_email_from_sqs_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_email_from_sqs_attach" {
  role       = "${aws_iam_role.lambda_email_from_sqs_assume_role.name}"
  policy_arn = "${aws_iam_policy.lambda_email_from_sqs_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "lambda_email_from_sqs_logs_attach" {
  role       = "${aws_iam_role.lambda_email_from_sqs_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}

data "aws_iam_policy_document" "lambda_email_from_sqs_permissions_policy" {
  statement {
    sid = "SSMPermissions${var.env}${var.app_version}"
    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters"
    ]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}",
      "${aws_ssm_parameter.dw_email_sqs.arn}",
      "${aws_ssm_parameter.ses_source_email_address.arn}",
      "${aws_ssm_parameter.dw_email_sqs.arn}",
      "${aws_ssm_parameter.ses_template_expiration_days_left.arn}"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:UpdateItem"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
  statement {
    sid = "SQSPermissions${var.env}${var.app_version}"
    actions = [
      "sqs:ChangeMessageVisibility",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:ReceiveMessage"
    ]
    resources = [
      "${aws_sqs_queue.ses_email_queue.arn}"
    ]
  }
  statement {
    sid       = "AllowInvokingLambdas"
    effect    = "Allow"
    resources = ["${aws_lambda_function.lambda_email_from_sqs.arn}"]
    actions   = ["lambda:InvokeFunction"]
  }
  statement {
    sid = "SESpermissions${var.env}${var.app_version}"
    actions = [
      "ses:SendTemplatedEmail"
    ]
    resources = ["*"]
  }
}

resource "aws_lambda_event_source_mapping" "lambda_email_from_sqs_source_mapping" {
  batch_size       = 1
  event_source_arn = "${aws_sqs_queue.ses_email_queue.arn}"
  enabled          = true
  function_name    = "${aws_lambda_function.lambda_email_from_sqs.arn}"
}

