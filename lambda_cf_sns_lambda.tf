
resource "aws_lambda_function" "cf_sns_lambda" {
  function_name    = "Cf_sns_lambda_${var.env}_${var.app_version}"
  s3_bucket        = "${aws_s3_bucket_object.lambda_code.bucket}"
  s3_key           = "${aws_s3_bucket_object.lambda_code.key}"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  handler          = "cf_sns_lambda.lambda_handler"
  runtime          = "python3.6"
  timeout          = 60
  role             = "${aws_iam_role.cf_sns_lambda_assume_role.arn}"
  depends_on       = ["aws_s3_bucket_object.lambda_code"]
}

/*
*   IAM
*/

data "aws_iam_policy_document" "cf_sns_lambda_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda_cf_sns_permissions_policy" {
  statement {
    sid = "SSMReadPermissions${var.env}${var.app_version}"
    actions = ["ssm:GetParameter"]
    resources = [
      "${aws_ssm_parameter.dw_dynamodb_table.arn}",
      "${aws_ssm_parameter.ses_template_on_creation_successful.arn}",
      "${aws_ssm_parameter.ses_template_on_creation_failed.arn}",
      "${aws_ssm_parameter.ses_source_email_address.arn}",
      "${aws_ssm_parameter.dw_email_sqs.arn}",
      "${aws_ssm_parameter.ses_template_on_deletion_successful.arn}",
      "${aws_ssm_parameter.ses_template_on_creation_successful_email_to_client.arn}",
      "${aws_ssm_parameter.dw_admin_emails.arn}",
      "${aws_ssm_parameter.password_prefix.arn}",
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
  statement {
    sid = "SSMDeletePermissions${var.env}${var.app_version}"
    actions = ["ssm:DeleteParameter"]
    resources = [
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
  statement {
    sid = "DynamoDBTablePermissions${var.env}${var.app_version}"
    actions = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
      "dynamodb:UpdateItem"
    ]
    resources = [
      "${data.aws_dynamodb_table.dynamodb_table.arn}"
    ]
  }
  statement {
    sid = "SESpermissions${var.env}${var.app_version}"
    actions = [
      "ses:SendTemplatedEmail"
    ]
    resources = ["*"]
  }
  statement {
    sid = "WorkspacesPermissions${var.env}${var.app_version}"
    actions = [
      "workspaces:DescribeWorkspaceDirectories"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda_cf_sns_policy" {
  name        = "lambda_cf_sns_policy_${var.env}_${var.app_version}"
  description = "Policy for Lambda Cf SNS, Digital Workforce"

  policy = "${data.aws_iam_policy_document.lambda_cf_sns_permissions_policy.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_cf_sns_attach" {
  role       = "${aws_iam_role.cf_sns_lambda_assume_role.name}"
  policy_arn = "${aws_iam_policy.lambda_cf_sns_policy.arn}"
}

resource "aws_iam_role" "cf_sns_lambda_assume_role" {
  name = "cf_sns_lambda_role_${var.env}_${var.app_version}"
  assume_role_policy = "${data.aws_iam_policy_document.cf_sns_lambda_assume_role_policy.json}"
  depends_on = ["aws_s3_bucket.lambdas_bucket"]
}

resource "aws_iam_role_policy_attachment" "cf_sns_lambda_logs_attach" {
  role       = "${aws_iam_role.cf_sns_lambda_assume_role.name}"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
}

resource "aws_lambda_permission" "cf_sns_lambda_allow_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.cf_sns_lambda.function_name}"
  principal     = "sns.amazonaws.com"
  source_arn    = "${aws_sns_topic.cloudformation_workspaces_sns_topic.arn}"
}
