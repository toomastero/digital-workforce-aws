resource "aws_cloudwatch_event_rule" "expiration_check_every_day" {
    name = "expiration_check_every_day_${var.env}_${var.app_version}"
    description = "Expiration check"
    schedule_expression = "cron(0 9 * * ? *)"
}

resource "aws_cloudwatch_event_target" "expiration_check_every_day_target" {
    rule = "${aws_cloudwatch_event_rule.expiration_check_every_day.name}"
    target_id = "expiration_check_every_day_${var.env}_${var.app_version}"
    arn = "${aws_lambda_function.lambda_expiration_checker.arn}"
}

resource "aws_cloudwatch_event_rule" "stack_termination_check_every_day" {
    name = "stack_termination_check_every_day_${var.env}_${var.app_version}"
    description = "Stack termination check"
    schedule_expression = "cron(0 17 * * ? *)"
}

resource "aws_cloudwatch_event_target" "stack_termination_check_every_day_target" {
    rule = "${aws_cloudwatch_event_rule.stack_termination_check_every_day.name}"
    target_id = "stack_termination_check_every_day_${var.env}_${var.app_version}"
    arn = "${aws_lambda_function.lambda_stack_termination.arn}"
}
