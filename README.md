
## Wiki documentation for this project

Please refer to: https://nordcloud.atlassian.net/wiki/spaces/NGGLOB/pages/1118011430/FI+-+Digital+Workforce+-+AWS+Workspaces


## Runbook for this project

Please refer to: https://nordcloud.atlassian.net/wiki/spaces/NGGLOB/pages/1202192427/Digital+Workforce+Runbook

