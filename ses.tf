
# This template is sent to admins
resource "aws_ses_template" "OnCreationSuccessful" {
  provider = "aws.ses_email_region"
  name    = "OnCreationSuccessful"
  subject = "New AWS Workspace machine"
  html    = <<EOL
<p>A new AWS Workspace is provisioned for: {{fullname}}</p>
EOL
}

# This template is sent to admins
resource "aws_ses_template" "OnCreationFailed" {
  provider = "aws.ses_email_region"
  name    = "OnCreationFailed"
  subject = "AWS Workspace provisioning failed"
  html    = <<EOL
<p>AWS Workspace provisioning failed for: {{fullname}}</p>
EOL
}

# This template is sent to admins
resource "aws_ses_template" "OnDeletionSuccessful" {
  provider = "aws.ses_email_region"
  name    = "OnDeletionSuccessful"
  subject = "AWS Workspace is deleted"
  html    = <<EOL
<p>An AWS Workspace is deleted for {{fullname}}</p>
EOL
}

# This template is sent to a client
resource "aws_ses_template" "WorkspaceExpirationEmail" {
  provider = "aws.ses_email_region"
  name    = "WorkspaceExpirationEmail"
  subject = "AWS Workspace is going to expire"
  html    = <<EOL
<h1>Hello {{fullname}},</h1>
<p>Your Workspace is going to expire in {{daysLeft}} days.</p>
<p>Please note that once the Workspace expires, it cannot be used anymore. If you wish to purchase an extension to your virtual desktop access, please contact info@dwfacademy.com</p>
EOL
}

# This template is sent to admins
resource "aws_ses_template" "EndDateUpdatedToAdmins" {
  provider = "aws.ses_email_region"
  name    = "EndDateUpdatedToAdmins"
  subject = "End date is updated for the Workspace"
  html    = <<EOL
<p>The end date is updated for user: {{useremail}}</p>
<p>New end date: {{endDate}}</p>
EOL
}

# This template is sent to a client
resource "aws_ses_template" "EndDateUpdatedToClient" {
  provider = "aws.ses_email_region"
  name    = "EndDateUpdatedToClient"
  subject = "End date for your Workspace is updated"
  html    = <<EOL
<h1>Hello {{fullname}},</h1>
<p>The end date for your Workspace is updated. Your workspace is going to expire now on {{endDate}}.</p>
EOL
}

# This template is sent to a client
resource "aws_ses_template" "OnCreationSuccessfulEmailToClient" {
  provider = "aws.ses_email_region"
  name    = "OnCreationSuccessfulEmailToClient"
  subject = "Your Amazon WorkSpace is created"
  html    = <<EOL
<h1>Hello {{fullname}},</h1>

<p>Your Amazon WorkSpace is ready.</p>

<p>1. Access your WorkSpace by downloading a client: https://clients.amazonworkspaces.com/</p>

<p>2. Enter the registration code: {{registrationCode}}</p>

<p>3. Your username is {{username}} and password: {{password}}</p>

<p>Please notice that you cannot change your password.</p>

<p>We recommend you to submit the final assignment 5 working days before the expiration day so you have time to make possible corrections if necessary.</p>

EOL
}
